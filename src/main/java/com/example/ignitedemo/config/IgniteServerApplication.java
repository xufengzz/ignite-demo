package com.example.ignitedemo.config;

import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.events.EventType;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.multicast.TcpDiscoveryMulticastIpFinder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * todo
 *
 * @author xufeng
 * @since 2024.04.30
 */
@Configuration
public class IgniteServerApplication {


    @Bean
    public Ignite  igniteInstance() {
        // If you provide a whole ClientConfiguration bean then configuration properties will not be used.
        // 1.创建一个Ignite配置

        IgniteConfiguration cfg = new IgniteConfiguration();
        // 开启事件监听 EVT_NODE_JOINED 加入集群  EVT_NODE_LEFT离开集群
        cfg.setIncludeEventTypes(EventType.EVT_CLUSTER_STATE_CHANGED,EventType.EVT_NODE_JOINED, EventType.EVT_NODE_LEFT);

        /*// 2.创建一个基于TCP的发现其他Ignite实例的Spi对象
        TcpDiscoverySpi discoverySpi = new TcpDiscoverySpi();
        // 3.创建一个组播IP探测器
        TcpDiscoveryMulticastIpFinder ipFinder = new TcpDiscoveryMulticastIpFinder();

        ipFinder.setMulticastGroup("192.168.191.1");
        ////设置IP发现器
        discoverySpi.setIpFinder(ipFinder);
        //设置Spi
        cfg.setDiscoverySpi(discoverySpi);*/

        // 4.启动Ignite服务器
        // Start a node.
        return Ignition.start(cfg);
    }

}
