package com.example.ignitedemo;

import jakarta.annotation.Resource;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class IgniteDemoApplication implements ApplicationRunner {

    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(IgniteDemoApplication.class);
/*        new SpringApplicationBuilder(IgniteDemoApplication.class)
                .web(WebApplicationType.NONE)
                .run(args);
        Thread.currentThread().join();*/
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

    }
}
